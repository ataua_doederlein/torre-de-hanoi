const interrogation = document.getElementById('interrogation');
const howTo = function () {
   alert(
      'Torre de Hanói.\n' +
      'O objetivo é transferir todos os discos da primeira fileira para a última.\n' +
      'Pode-se deslocar cada disco para um espaço vazio ou para cima de outro disco maior\n' +
      '(ou seja, um disco não pode ficar sobre outro menor.)'
   );
}
interrogation.addEventListener('click', howTo);

let main = document.getElementsByTagName('main')[0];
let moves = document.getElementById('moves');

function criaVaretas(...ids) {
   for (id of ids) {
      let vareta = document.createElement('section');
      vareta.setAttribute('id', id);
      vareta.setAttribute('class', 'vareta');
      main.appendChild(vareta);
   }
}
criaVaretas('inicio', 'meio', 'fim');

function criaDiscos(...ids) {
   let main = document.getElementById('inicio');
   for (id of ids) {
      let disco = document.createElement('div');
      disco.setAttribute('id', 'disco' + id);
      disco.setAttribute('class', 'disco');
      main.appendChild(disco);
   }
}
criaDiscos(1, 2, 3, 4);

let inicio = document.getElementById('inicio');
let meio = document.getElementById('meio');
let fim = document.getElementById('fim');
let varetas = [inicio, meio, fim];

let discoAtual;
let origem;
let start = false;

let body = document.getElementsByTagName('body')[0];

function compvitoria() {
   let content = fim.childNodes;
   if (content[0] === disco1 &&
      content[1] === disco2 &&
      content[2] === disco3 &&
      content[3] === disco4) {
      vitoria()
   }
}

function vitoria() {

   main.removeChild(inicio)
   main.removeChild(meio)
   main.removeChild(fim)
   // body.removeChild(document.getElementById('movimentos'))

   let ttitulo = document.createTextNode('Parabéns Você ganhou!!!!')
   let titulo = document.createElement('h1')
   titulo.style.color = 'red'
   titulo.appendChild(ttitulo)

   let tsubtitulo = document.createTextNode("Você completou o jogo em " + moves.textContent + ' movimentos')
   let subtitulo = document.createElement('h2')
   subtitulo.appendChild(tsubtitulo)

   let trecomecar = document.createTextNode('pressione F5 para jogar novamente')
   let recomecar = document.createElement('h3')
   recomecar.appendChild(trecomecar)

   body.appendChild(titulo)
   body.appendChild(subtitulo)
   body.appendChild(recomecar)
}

for (vareta of varetas) {
   vareta.addEventListener('click', function (event) {

      if (start === false) {
         // seleciona vareta de origem
         start = true;
         origem = this;
         let pilha = origem.childElementCount;
         // se não tiver discos nessa pilha, cancela selação
         if (pilha === 0) {
            start = false;
         }
         // seleciona o disco de cima da pilha
         discoAtual = origem.lastElementChild;
         discoAtual.classList.add('selected');
         return origem;
      }

      if (!!start) {
         // seleciona destino
         let destino = this;
         // se for a mesma vareta, cancela movimento
         if (destino === origem) {
            discoAtual.classList.remove('selected');
            start = false;
         }
         // verifica se já tem discos no local
         let pilha = destino.childElementCount;
         if (pilha !== 0) { // pilha não está vazia
            let discoTopo = destino.lastElementChild;
            if (discoTopo.offsetWidth < discoAtual.offsetWidth) { // compara os tamanhos. se o item do topo for menor, cancela o movimento
               discoAtual.classList.remove('selected');
               start = false;
            }
         }
         if (start === true) { // tá valendo (condições de cancelamento anteriores não satisfeitas)
            destino.appendChild(discoAtual);
            discoAtual.classList.remove('selected');
            discoAtual = null;
            moves.textContent++;
            start = false;
            compvitoria();
         }

      }
   });
}